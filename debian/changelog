testssl.sh (3.2~rc4+dfsg-1) unstable; urgency=medium

  * New upstream version 3.2~rc4+dfsg.
    - Refresh patch.
  * d/control: Fix depend, dnsutils → bind9-dnsutils. (Closes: #1094776)
  * d/copyright: Update my years.
  * Update Standards-Version to 4.7.0.

 -- Unit 193 <unit193@debian.org>  Sat, 01 Feb 2025 00:24:18 -0500

testssl.sh (3.2~rc3+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

  [ Unit 193 ]
  * New upstream version 3.2~rc3+dfsg.
    - Refresh patch.
  * d/control: Drop old version constraints and update recommends
  * d/copyright: Update my years.
  * d/rules: Drop dh_fixperms override, no longer needed.
  * Update Standards-Version to 4.6.2.

 -- Unit 193 <unit193@debian.org>  Sat, 04 Nov 2023 19:11:24 -0400

testssl.sh (3.0.8+dfsg-1) unstable; urgency=medium

  * d/watch: Switch from using GitHub releases to tags.
  * New upstream version 3.0.8+dfsg.
    - Refresh patch.
  * Update Standards-Version to 4.6.1.

 -- Unit 193 <unit193@debian.org>  Wed, 02 Nov 2022 05:51:29 -0400

testssl.sh (3.0.7+dfsg-1) unstable; urgency=medium

  * d/watch: Drop the number off dfsg version.
  * New upstream version 3.0.7+dfsg.
    - Refresh patch.
  * d/t/upstream-tests.sh: Add upstream tests for autopkgtest.
  * d/copyright: Add my attribution.
  * d/rules: client-simulation.txt is sourced, not executed, drop execute bit.

 -- Unit 193 <unit193@debian.org>  Sun, 20 Feb 2022 19:32:08 -0500

testssl.sh (3.0.6+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.0.6+dfsg1.
    - Refresh patch.
  * Update Standards-Version to 4.6.0.

 -- Unit 193 <unit193@debian.org>  Sun, 10 Oct 2021 02:53:10 -0400

testssl.sh (3.0.5+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.0.5+dfsg1.
    - Refresh patch.
  * d/control: Bump DH compat to 13.

 -- Unit 193 <unit193@debian.org>  Wed, 11 Aug 2021 18:54:00 -0400

testssl.sh (3.0.4+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.0.4+dfsg1.
    - Refresh patch.

 -- Unit 193 <unit193@debian.org>  Sun, 22 Nov 2020 19:46:09 -0500

testssl.sh (3.0.3+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.0.3+dfsg1.
    - Refresh patch.
  * d/watch: Needlessly bump compat to 4.
  * Update Standards-Version to 4.5.1.

 -- Unit 193 <unit193@debian.org>  Thu, 19 Nov 2020 22:42:46 -0500

testssl.sh (3.0.2+dfsg1-3) unstable; urgency=medium

  * d/control: Correct dependancies for backportability.
  * Adjust line exceeding 80 columns in previous changelog entry.

 -- Unit 193 <unit193@debian.org>  Tue, 28 Jul 2020 02:35:25 -0400

testssl.sh (3.0.2+dfsg1-2) unstable; urgency=medium

  * Team upload.

  [ Gerardo Di Giacomo ]
  * d/control: Update binary dependencies. Closes: #962995

  [ Raphaël Hertzog ]
  * Set upstream metadata fields: Bug-Database,
    Bug-Submit, Repository, Repository-Browse.

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 29 Jun 2020 22:56:26 +0200

testssl.sh (3.0.2+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.0.2+dfsg1.
    - Refresh patch.
  * d/control: Update my email address.
  * d/copyright: Drop license comment as wording has been removed upstream.

 -- Unit 193 <unit193@debian.org>  Fri, 08 May 2020 20:05:24 -0400

testssl.sh (3.0.1+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.0.1+dfsg1
    - Refresh patch.

 -- Unit 193 <unit193@ubuntu.com>  Sun, 19 Apr 2020 19:38:40 -0400

testssl.sh (3.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.0+dfsg1
    - Refresh patch.
  * d/control: Add myself to uploaders.
  * d/s/local-options: Drop, as these are default.
  * Update Standards-Version to 4.5.0.

 -- Unit 193 <unit193@ubuntu.com>  Wed, 29 Jan 2020 17:53:47 -0500

testssl.sh (3.0~rc6+dfsg1-1) unstable; urgency=medium

  * Team upload.

  [ Samuel Henrique ]
  * Add salsa-ci.yml

  [ Unit 193 ]
  * d/watch: Update to pick up RC tags.
  * New upstream version 3.0~rc6+dfsg1
    - Refresh patch.
  * d/rules: Update for renamed changelog file.
  * d/compat, d/control: Drop d/compat in favor of debhelper-compat.
  * d/control: Set R³ to no.
  * Update Standards-Version to 4.4.1.
  * d/testssl.sh.maintscript: Clean up obsolete config file.
  * d/copyright: Add upstream license note as a comment.

 -- Unit 193 <unit193@ubuntu.com>  Mon, 06 Jan 2020 23:39:33 -0500

testssl.sh (2.9.5-7+dfsg1-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.9.5-7+dfsg1
  * Bump Debhelper compat level
  * Bump Standards-Version
  * Update patch

 -- Hilko Bengen <bengen@debian.org>  Sun, 27 Jan 2019 22:25:45 +0100

testssl.sh (2.9.5-5+dfsg1-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>.

  [ ChangZhuo Chen (陳昌倬) ]
  * New upstream release.
  * Bump Standards-Version to 4.1.5.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 17 Jul 2018 13:07:55 +0800

testssl.sh (2.9.5-1+dfsg1-2) unstable; urgency=medium

  [ Unit 193 ]
  * d/p/allow_loading_config.patch:
    - Config is presumed to be in the same dir as the script, or in ./etc/
  * d/install: Install etc/* to /etc/testssl. (Closes: #888393)

  [ ChangZhuo Chen (陳昌倬) ]
  * Bump Standards-Version to 4.1.3.
  * Bump compat to 11.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 11 Feb 2018 21:22:57 +0800

testssl.sh (2.9.5-1+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.1.
    * Change Priority to optional.
    * Change Format in copyright to https.
  * Add Multi-Arch: foreign.
  * Use upstream manpage.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Fri, 06 Oct 2017 18:57:29 +0800

testssl.sh (2.8~rc3+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Change maintainer to Debian Security Tools Packaging Team.
  * Update Vcs-* fields.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Fri, 06 Jan 2017 15:48:31 +0800

testssl.sh (2.6+dfsg1-2) unstable; urgency=medium

  * Fix FTBFS
  * Install upstream changelog
  * Update manpage
    * Replace testssl.sh with testssl

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 15 Oct 2015 11:54:48 +0800

testssl.sh (2.6+dfsg1-1) unstable; urgency=low

  * Initial release. Closes: #800055

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 27 Sep 2015 22:38:29 +0800
